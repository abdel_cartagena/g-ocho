<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
//MODELS
use App\Models\Categorias;

class ModuloCategorias extends Component
{

	use WithPagination;

	public $search;
	public $perPage = '5';
	public $ifOpenModalCategorias = false;
	public $ifOpenDeleteCategorias = false;
	public $categoriasInput = Categorias::class;
	public $create = true; // edit = false

	protected $rules = [
		'categoriasInput.id_padre'=>'nullable|exists:categorias,id',
		'categoriasInput.nombre' => 'required|max:200',
	];
	protected $messages = [
		'id.exists' => 'El id debe existir en la tabla de categorias.',
		'required' => 'El campo :attribute es requerido.',
		'numeric' => 'El campo :attribute debe ser un numero',
		'unique' => 'El :attribute :input ya existe en la tabla de categorias.',
		'max' => 'El :attribute no debe tener más de :max caracteres.'
	];

    public function render()
    {
		$categorias = Categorias::where('nombre', 'like', "%{$this->search}%")
			->orWhere('id_padre', 'like', "%{$this->search}%")
			->with('padre');
        return view('livewire.categorias.categoriasView', [
			'categorias' => $categorias->paginate($this->perPage),
			'padres' => Categorias::get()
		]);
	}

	public function createCategoria()
	{
		$this->validate();
		Categorias::create($this->categoriasInput);
		$this->categoriasInput = Categorias::class;
		$this->ifOpenModalCategorias = false;
		$this->notify('Categoria creada con exito');
	}

	public function updateCategoria()
	{
		$this->validate();
		$this->categoriasInput->save();
		$this->categoriasInput = Categorias::class;
		$this->ifOpenModalCategorias = false;
		$this->notify('Categoria actualizada con exito');
	}

	public function deleteCategoria()
	{
		$this->categoriasInput->delete();
		$this->categoriasInput = Categorias::class;
		$this->ifOpenDeleteCategorias = false;
		$this->notify('Categoria eliminada con exito');
	}

	public function showCreateCategoria()
	{
		$this->ifOpenModalCategorias = true;
		$this->categoriasInput = Categorias::class;
		$this->create = true;
	}

	public function showEditCategoria(Categorias $categoria)
    {
		$this->categoriasInput = $categoria;
		$this->ifOpenModalCategorias = true;
		$this->create = false;
	}

	public function showDeleteCategoria(Categorias $categoria)
	{
		$this->categoriasInput = $categoria;
		$this->ifOpenDeleteCategorias = true;
	}

	public function closeModalsCategoria()
	{
		$this->ifOpenModalCategorias = false;
		$this->ifOpenDeleteCategorias = false;
	}

}