<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\User;
use App\Models\tbEps;
use App\Models\tbRoles;
use Illuminate\Support\Facades\Hash;

use DateTime;

class Usuarios extends Component
{

	public $usuario = User::class;
	public $ifOpenModalUsuarios = false;
	public $ifOpenDeleteUsuario = false;
	public $create;

	protected $rules = [
        'usuario.name' => 'required',
		'usuario.email' => 'required|email',
		// 'usuario.password' => 'required',
		'usuario.documento' => 'required',
		'usuario.genero' => 'required',
		'usuario.fecha_nacimiento' => 'required',
		'usuario.telefono' => 'required',
		'usuario.eps_id' => 'required',
		'usuario.rol_id' => 'required',
    ];

    public function render()
    {
        return view('livewire.usuarios', [
			'usuarios' => User::with(['eps','rol'])->get(),
			'roles' => tbRoles::get(),
			'eps' => tbEps::get(),
		]);
	}

	public function showCreateUsuarios()
	{
		$this->usuario = User::class;
		$this->ifOpenModalUsuarios = true;
		$this->create = true;
	}

	public function closeModalsUsuarios()
	{
		$this->ifOpenModalUsuarios = false;
	}

	public function createUsuarios()
	{
		$this->validate();
		$password = Hash::make($this->usuario['password']);
		$this->usuario['password'] = $password;
		User::create($this->usuario);
		$this->ifOpenModalUsuarios = false;
		$this->notify('Usuario creado con exito');
	}

	public function showEditUsuario(User $usuario)
	{
		$this->usuario = $usuario;
		$this->ifOpenModalUsuarios = true;
		$this->create = false;
	}

	public function updateUsuarios()
	{
		$usuario = User::whereId($this->usuario['id'])->first();
		if($usuario->password != $this->usuario['password']){
			$password = Hash::make($this->usuario['password']);
			$this->usuario['password'] = $password;
		}else{
			$this->usuario['password'] = $usuario->password;
		}
		$this->usuario->save();
		$this->ifOpenModalUsuarios = false;
		$this->notify('Usuario actualizado con exito');
	}

	public function showDeleteUsuario(User $usuario)
	{
		$this->ifOpenDeleteUsuario = true;
		$this->usuario = $usuario;
	}

	public function closeModalsUsuario()
	{
		$this->ifOpenDeleteUsuario = false;
		$this->ifOpenModalUsuarios = false;
	}

	public function deleteUsuario()
	{
		$this->usuario->delete();
		$this->closeModalsUsuario();
		$this->notify('Usuario eliminado con exito');
	}
}
