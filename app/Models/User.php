<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;
use DateTime;
class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
		'password',
		'documento',
		'genero',
		'fecha_nacimiento',
		'telefono',
		'eps_id',
		'rol_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
	];

	public function eps()
    {
        return $this->hasOne('App\Models\tbEps', 'id',  'eps_id');
	}

	public function rol()
    {
        return $this->hasOne('App\Models\tbRoles', 'id',  'rol_id');
	}

	public function getEdadAttribute()
	{
		$date1 = new DateTime($this->fecha_nacimiento);
		$date2 = new DateTime("now");
		$diff = $date1->diff($date2);


		return $diff->y;
	}
}
