## BACKEND
- [Laravel 8](https://laravel.com/docs/routing).
- [Livewire](ttps://laravel-livewire.com/).
## FRONT
- [tailwindcss](https://tailwindcss.com/).
- [alpinejs](https://alpinejs.dev/).
- [vuejs](https://vuejs.org/).
## DATABASE
- [mysql](https://www.mysql.com/).