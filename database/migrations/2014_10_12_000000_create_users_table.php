<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
			$table->string('name');
			$table->char('documento', 100)->nullable();
			$table->char('genero', 100)->nullable();
			$table->date('fecha_nacimiento')->nullable();
			$table->char('telefono', 100)->nullable();
			$table->unsignedBigInteger('eps_id')->nullable();
			$table->foreign('eps_id')->references('id')->on('tb_eps');
			$table->unsignedBigInteger('rol_id')->nullable();
    		$table->foreign('rol_id')->references('id')->on('tb_roles');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->foreignId('current_team_id')->nullable();
            $table->text('profile_photo_path')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
