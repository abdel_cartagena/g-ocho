<x-jet-dialog-modal wire:model="ifOpenModalUsuarios">

	<x-slot name="title">
		@if ($create)
			Crear usuario
		@else
			Editar usuario
		@endif
	</x-slot>


	<x-slot name="content">

		@if (session()->has('message'))
			<div class="alert alert-success">
				{{ session('message') }}
			</div>
		@endif

			<div>
				<x-jet-label for="rol" value="{{ __('Rol') }}" />
				<select wire:model="usuario.rol_id" id="rol_id" name="rol_id" class="mt-1 block border border-gray-300 bg-white rounded-md text-gray-500 w-full">
					<option value="">Seleccionar</option>
					@foreach($roles as $rol)
						<option value="{{ $rol->id }}">{{ $rol->nombre }}</option>
					@endforeach
				</select>
				@error('usuario.rol_id') <span class="error text-red-600">{{ $message }}</span> @enderror
			</div>

			<div class="mt-2">
				<x-jet-label for="eps" value="{{ __('Eps') }}" />
				<select wire:model="usuario.eps_id" id="eps_id" name="eps_id" class="mt-1 block border border-gray-300 bg-white rounded-md text-gray-500 w-full">
					<option value="">Seleccionar</option>
					@foreach($eps as $ep)
						<option value="{{ $ep->id }}">{{ $ep->nombre }}</option>
					@endforeach
				</select>
				@error('usuario.eps_id') <span class="error text-red-600">{{ $message }}</span> @enderror
			</div>

			<div class="mt-2">
				<x-jet-label for="nombre" value="{{ __('Nombre') }}" />
				<x-jet-input wire:model="usuario.name" id="name" name="name" class="block text-gray-500 mt-1 w-full" type="text" />
				@error('usuario.name') <span class="error text-red-600">{{ $message }}</span> @enderror
			</div>

			<div class="mt-2">
				<x-jet-label for="email" value="{{ __('Email') }}" />
				<x-jet-input wire:model="usuario.email" id="email" name="email" class="block text-gray-500 mt-1 w-full" type="text" />
				@error('usuario.email') <span class="error text-red-600">{{ $message }}</span> @enderror
			</div>

			<div class="mt-2">
				<x-jet-label for="password" value="{{ __('Password') }}" />
				<x-jet-input wire:model="usuario.password" id="password" name="password" class="block text-gray-500 mt-1 w-full" type="text" />
				@error('usuario.password') <span class="error text-red-600">{{ $message }}</span> @enderror
			</div>

			<div class="mt-2">
				<x-jet-label for="documento" value="{{ __('Documento') }}" />
				<x-jet-input wire:model="usuario.documento" id="name" name="name" class="block text-gray-500 mt-1 w-full" type="text" />
				@error('usuario.documento') <span class="error text-red-600">{{ $message }}</span> @enderror
			</div>

			<div class="mt-2">
				<x-jet-label for="genero" value="{{ __('Genero') }}" />
				<select wire:model="usuario.genero" id="genero" name="genero" class="mt-1 block border border-gray-300 bg-white rounded-md text-gray-500 w-full">
					<option value="">Seleccionar</option>
					<option value="masculino">Masculino</option>
					<option value="femenino">Femenino</option>
				</select>
				@error('usuario.genero') <span class="error text-red-600">{{ $message }}</span> @enderror
			</div>

			<div class="mt-2">
				<x-jet-label for="fecha_nacimiento" value="{{ __('Fecha nacimiento') }}" />
				<x-jet-input wire:model="usuario.fecha_nacimiento" id="fecha_nacimiento" name="fecha_nacimiento" class="block text-gray-500 mt-1 w-full" type="text" />
				@error('usuario.fecha_nacimiento') <span class="error text-red-600">{{ $message }}</span> @enderror
			</div>

			<div class="mt-2">
				<x-jet-label for="telefono" value="{{ __('Telefono') }}" />
				<x-jet-input wire:model="usuario.telefono" id="telefono" name="telefono" class="block text-gray-500 mt-1 w-full" type="text" />
				@error('usuario.telefono') <span class="error text-red-600">{{ $message }}</span> @enderror
			</div>

	</x-slot>

	<x-slot name="footer">

		<x-jet-danger-button wire:click="closeModalsUsuarios">
			{{ __('Cancelar') }}
		</x-jet-danger-button>

		<x-button.primary wire:click='createUsuarios' class="ml-4" wire:loading.attr="disabled" hidden="{{!$create}}">
			{{ __('Crear usuario') }}
		</x-button.primary>

		<x-button.primary wire:click='updateUsuarios' class="ml-4" wire:loading.attr="disabled" hidden="{{$create}}">
			{{ __('Editar usuario') }}
		</x-button.primary>

	</x-slot>

</x-jet-dialog-modal>