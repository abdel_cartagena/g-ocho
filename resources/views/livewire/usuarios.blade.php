<div class="p-5">

	<div class="max-w-7xl mx-auto sm:px-6 lg:px-8 py-2">
		<x-jet-button wire:click="showCreateUsuarios">
			{{ __('Crear usuarios') }}
		</x-jet-button>
	</div>

	@include('livewire.usuariosForm')

	<x-modal.confirmation wire:model="ifOpenDeleteUsuario" maxWidth="sm" class="h-5/6">

		<x-slot name="title">
			¿Desea eliminar este usuario?
		</x-slot>

		<x-slot name="content">
			Este proceso no se podrá revertir!
		</x-slot>

		<x-slot name="footer">

			<x-button.danger wire:click="closeModalsUsuario">
				{{ __('Cancelar') }}
			</x-button.danger>

			<x-button.primary wire:click='deleteUsuario' class="ml-4" wire:loading.attr="disabled">
				{{ __('Borrar Usuario') }}
			</x-button.primary>

		</x-slot>

	</x-modal.confirmation>

	<!-- This example requires Tailwind CSS v2.0+ -->
	<div class="flex flex-col">
		<div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
		<div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
			<div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
			<table class="min-w-full divide-y divide-gray-200">
				<thead class="bg-gray-50">
				<tr>
					<th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
					Nombre
					</th>
					<th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
					Documento
					</th>
					<th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
					ROL
					</th>
					<th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
					EPS
					</th>
					<th scope="col" class="relative px-6 py-3">
					</th>
				</tr>
				</thead>
				<tbody class="bg-white divide-y divide-gray-200">

					@foreach ($usuarios as $usuario)

					@if ($usuario->edad > 50)
						<tr class="bg-red-200">
					@elseif ($usuario->edad < 18)
						<tr class="bg-green-200">
					@else
						<tr>
					@endif

							<td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
								{{ $usuario->name }}
							</td>
							<td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
								{{ $usuario->documento }}
							</td>
							<td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
								{{ $usuario->rol->nombre }}
							</td>
							<td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
								{{ $usuario->eps->nombre }}
							</td>
							<td class="px-6 py-4 text-right text-sm font-medium">
								<x-button.link wire:click="showEditUsuario({{ $usuario->id }})">
									Editar
								</x-button.link>
								<x-button.link wire:click="showDeleteUsuario({{ $usuario->id }})" class="text-red-500 ml-3">
									Eliminar
								</x-button.link>
							</td>
						</tr>

					@endforeach

				<!-- More people... -->
				</tbody>
			</table>
			</div>
		</div>
		</div>
	</div>
</div>
